import http from 'node:http'

const users = []

const server = http.createServer((request, response) => {
    const { method, url } = request

    if (method === 'GET' && url === '/users') {
        return response
            .setHeader('Content-Type', 'application/json')
            .end(JSON.stringify(users))
    }

    if (method === 'POST' && url === '/users') {
        users.push({
            name: 'John Doe',
            email: 'jogndoe@example.com'
        })

        return response.writeHead(201).end()
    }

    return response.writeHead(408).end()
})

server.listen(3000)