# Portfolio

## Code
/*
Crie um programa em linguagem C que utilize arrays e estruturas de repetição para armazenar os códigos, nomes e preços de itens.

O programa deve:

** Deve ter:

** Code(id)
** Name
** Quantity
** Unity value
** Total

1 - Solicitar ao usuário o número de itens que ele deseja inserir.

2 - Armazenar os ids, nomes e preços e quantidade dos itens em arrays.

3 - Não permitir que lance um item com o mesmo código já inserido.

4 - Imprimir uma lista dos itens com seus respectivos preços.

5 - Calcular e imprimir o total dos preços de todos os itens.
O envio de mensagens se encerra em 31/05/2024

Meu GitHub: https://github.com/HenriqueStocco?tab=repositories
*/

#include <stdio.h>
#include <string.h>

int main()
{
    int code, quantity, length;
    float unity, total;
    char name[10];
    char array[10] = {};

    struct Product
    {
        int code;
        char name[10];
        int quantity;
        float unity
    };

    length = 4;

    struct Product product;

    printf("Welcome!\n");
    printf("Enter the code of product: \n");
    scanf("%d", &code);

    product.code = code;

    printf("Enter the name of product: \n");
    scanf("%s", name);

    strcpy(product.name, name);

    printf("Enter the quantity of products will have: \n");
    scanf("%d", &quantity);

    product.quantity = quantity;

    printf("Enter the price of each unity of products: \n");
    scanf("%f", &unity);

    product.unity = unity;

    return 0;